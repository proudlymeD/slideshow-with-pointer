# slideshow-with-pointer
### Last updated: 11/02/2022

A very, very basic slideshow viewer wrapper script. 

To use, just prepare a directory of readable images and then run the script. 

Flags: use -i for directory, -s for highlighter size, -r|-p for released/pressed colors (hex code, e.g. #000000). 


### Require

`feh` [GitHub](https://github.com/derf/feh)

`highlight-pointer` [GitHub](https://github.com/swillner/highlight-pointer)

